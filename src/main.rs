use anyhow::{anyhow, Result};
use reqwest::blocking::Client;
use serde::Deserialize;
use std::{env, vec::Vec};

#[derive(Deserialize, Debug)]
struct CityData {
    name: String,
}

#[derive(Deserialize, Debug)]
struct AQIData {
    aqi: u32,
    city: CityData,
}

#[derive(Deserialize, Debug)]
struct APIData {
    data: AQIData,
}

fn fetch_aqi(token: String, city: &str) -> Result<APIData> {
    let url = format!(
        "http://api.waqi.info/feed/{}/?token={}",
        city,
        token,
    );
    let resp = Client::new().get(&url).send()?;

    match resp.json::<APIData>() {
        Ok(d) => Ok(d),
        Err(_) => Err(anyhow!("Failed to process the data")),
    }
}

fn match_level(aqi: u32) -> &'static str {
    match aqi {
        0..=50 => "Good",
        51..=100 => "Moderate",
        101..=150 => "Unhealthy for Sensitive Groups",
        151..=200 => "Unhealthy",
        201..=300 => "Very Unhealthy",
        _ => "Hazardous",
    }
}

fn match_level_simple(aqi: u32) -> &'static str {
    match aqi {
        0..=50 => "G",
        51..=100 => "M",
        101..=150 => "S",
        151..=200 => "U",
        201..=300 => "V",
        _ => "H",
    }
}

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let city = args
        .get(1)
        .ok_or_else(|| anyhow!("Please enter a city name"))?;
    let fetched = fetch_aqi(env::var("TOKEN")?, city)?;
    let simple: bool = env::var("SIMPLE").unwrap_or_else(|_| "false".to_owned()).parse().unwrap_or(false);

    if simple {
        print!(
            "{} ({})",
            fetched.data.aqi,
            match_level_simple(fetched.data.aqi)
        );
    } else {
        println!(
            "{}: {} ({})",
            fetched.data.city.name,
            fetched.data.aqi,
            match_level(fetched.data.aqi)
        );
    }

    Ok(())
}
